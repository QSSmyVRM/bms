﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
using System.Xml;
using System.Web.UI.HtmlControls;

namespace en_SearchBridgeConference
{

    public partial class SearchBridgeConference : System.Web.UI.Page
    {

        # region prviate DataMember

        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        static int totalPages;
        static int pageNo;
        DataSet ds = new DataSet();
        ns_Logger.Logger log;
        protected string language = "";
        string SortingOrder = "0";
        bool isViewUser = false;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAscendingOrder;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSortBy;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSortingOrder;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnShowpopup;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.WebControls.Table tblNoMCUs;
        protected System.Web.UI.WebControls.DataGrid dgConferenceList;
        protected System.Web.UI.WebControls.DataGrid dgMCUs;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoConferences;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;
        protected System.Web.UI.WebControls.DropDownList lstBridgeStatus;
        protected System.Web.UI.WebControls.CheckBox chkAllPages;
        protected System.Web.UI.WebControls.CheckBox chkSelectPage;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPages;
        protected System.Web.UI.HtmlControls.HtmlButton btnCancel;
        //ZD 101233 Starts
        protected int hasView = 0;
        protected int hasEdit = 0;
        protected int hasDelete = 0;
        protected int hasManage = 0;
        protected int hasClone = 0;
        protected int hasMCUInfo = 0;
        protected int hasExtendTime = 0;
        //ZD 101233 End
        #endregion

        public SearchBridgeConference()
        {
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheckNoSession(Request.Url.AbsoluteUri.ToLower());

                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                isViewUser = obj.ViewUserRole();
                if (hdnAscendingOrder.Value == "1")
                    SortingOrder = "1";
                else
                    SortingOrder = "0";

                if (!IsPostBack)
                {
                    errLabel.Text = "";
                    BindDataFromSearch();
                    BindData();
                }

                if (Session["admin"].ToString() != "2" && Session["admin"].ToString() != "3")
                    trPages.Visible = false;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);
            }
        }
        #endregion

        #region BindDataFromSearch
        /// <summary>
        /// BindDataFromSearch
        /// </summary>
        protected void BindDataFromSearch()
        {
            try
            {
                string pageNoS = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNoS = Request.QueryString["pageNo"].ToString().Trim();
                XmlDocument xmldocInXML = new XmlDocument();
                xmldocInXML.LoadXml(Session["ChangeinXML"].ToString());
                string pageNoInInXML = xmldocInXML.SelectSingleNode("//SearchConference/PageNo").InnerText;
                string sortBy = xmldocInXML.SelectSingleNode("//SearchConference/SortBy").InnerText;
                string SortingValue = xmldocInXML.SelectSingleNode("//SearchConference/SortingOrder").InnerText;
                Session["ChangeinXML"] = Session["ChangeinXML"].ToString().Replace("<PageNo>" + pageNoInInXML + "</PageNo>", "<PageNo>" + pageNoS + "</PageNo>");
                if (Request.QueryString["sortBy"] != null)
                {
                    if (Request.QueryString["sortBy"].ToString().Equals(txtSortBy.Value) || txtSortBy.Value.Equals(""))
                        txtSortBy.Value = Request.QueryString["sortBy"].ToString();
                    else
                        txtSortBy.Value = txtSortBy.Value;
                }
                if (!txtSortBy.Value.Trim().Equals(""))
                    Session["ChangeinXML"] = Session["ChangeinXML"].ToString().Replace("<SortBy>" + sortBy + "</SortBy>", "<SortBy>" + txtSortBy.Value + "</SortBy>");

                Session["ChangeinXML"] = Session["ChangeinXML"].ToString().Replace("<SortingOrder>" + SortingValue + "</SortingOrder>", "<SortingOrder>" + SortingOrder + "</SortingOrder>");

                string outXML = obj.CallMyVRMServer("SearchConference", Session["ChangeinXML"].ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    dgConferenceList.DataSource = null;
                    dgConferenceList.DataBind();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                    LoadConferenceList(nodes, dgConferenceList);
                    if (nodes.Count > 0)
                    {
                        ShowHide();
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgConferenceList.Controls[0].Controls[dgConferenceList.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text = xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText;

                        totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//SearchConference/TotalPages").InnerText);
                        pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//SearchConference/PageNo").InnerText);
                        if (totalPages > 1)
                        {
                            string qString = Request.QueryString.ToString();
                            if (Request.QueryString["pageNo"] != null)
                                qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                            if (Request.QueryString["m"] != null)
                                qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                            obj.DisplayPaging(totalPages, pageNo, tblPage, "searchbridgeconference.aspx?" + qString);
                            DisplaySort();
                        }
                        foreach (DataGridItem dgi in dgConferenceList.Items)
                        {
                            if ((LinkButton)dgi.FindControl("btnViewDetails") != null)
                                ((LinkButton)dgi.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:viewconf('" + dgi.Cells[0].Text + "');return false;");
                        }
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindDataFromSearch:" + ex.Message);
            }
        }
        #endregion

        #region InterpretRole
        /// <summary>
        /// InterpretRole
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterpretRole(Object sender, DataGridItemEventArgs e)
        {
            
            //Label lblChangeMCU = null;
            CheckBox chkChangeMCU = null;
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.SelectedItem) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //lblChangeMCU = (Label)e.Item.FindControl("lblChangeMCU");
                    chkChangeMCU = (CheckBox)e.Item.FindControl("chkChangeMCU");
                    //lblChangeMCU.Visible = false;
                    chkChangeMCU.Visible = false;

                    if (Session["admin"].ToString() == "2" || Session["admin"].ToString() == "3")
                    {
                        //lblChangeMCU.Visible = true;
                        chkChangeMCU.Visible = true;   
                    }

                    if (e.Item.Cells[4].Text.Equals("7"))  //Past Conferences
                    {
                        LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                        btnTemp.Visible = false;
                        //lblChangeMCU.Visible = false;
                        chkChangeMCU.Visible = false;  
                    }
                    else if (e.Item.Cells[4].Text.Equals("5")) //Ongoing Conference 
                    {
                        LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                        btnTemp.Visible = false;
                        //lblChangeMCU.Visible = false;
                        chkChangeMCU.Visible = false;  
                    }
                    else if (e.Item.Cells[4].Text.Equals("9")) //Deleted
                    {
                        LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                        btnTemp.Visible = false;
                        //lblChangeMCU.Visible = false;
                        chkChangeMCU.Visible = false;
                    }
                    else if (e.Item.Cells[4].Text.Equals("6")) //On MCU //ZD 100036
                    {
                        LinkButton  btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                        btnTemp.Visible = false;
                        //lblChangeMCU.Visible = false;
                        chkChangeMCU.Visible = false; 
                    }
					//ZD 101388 Commented Starts
                    /*if (Session["isExpressUser"] != null)
                    {
                        if (Session["isExpressUser"].ToString() == "1")
                        {
                            LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                            btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnManage");
                            btnTemp.Visible = false;

                            if (Session["isExpressUserAdv"].ToString() == "1")
                            {
                                if (Request.QueryString["t"] != null)
                                {
                                    if (Request.QueryString["t"].ToString() == "2")
                                    {
                                        btnTemp.Visible = true;

                                    }
                                }
                            }

                        }
                    }
                    if (isViewUser)
                    {
                        if (Session["isExpressUser"] != null)
                        {
                            if (Session["isExpressUser"].Equals("0"))
                            {
                                LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnManage");
                                btnTemp.Visible = false;
                            }
                        }
                    }*/
					//ZD 101388 Commented End
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("InterpretRole:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region LoadConferenceList
        /// <summary>
        /// LoadConferenceList
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="dgList"></param>
        protected void LoadConferenceList(XmlNodeList nodes, DataGrid dgList)
        {
            try
            {
                XmlTextReader xtr;
                ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    if (node.InnerXml.Trim().IndexOf("<Selected>") <= 0)
                    {
                        XmlNode nodeLoc = node.SelectSingleNode("//SearchConference/Conferences/Conference/Location");
                        nodeLoc.InnerXml = "<Selected><ID>-1</ID><Name>Other</Name><isVMR>0</isVMR></Selected>";
                    }
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.Auto);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    {
                        if (!dv.Table.Columns.Contains("Locations"))
                            dv.Table.Columns.Add("Locations");
                        if (!dv.Table.Columns.Contains("ConferenceType"))
                            dv.Table.Columns.Add("ConferenceType");
                        if (!ds.Tables[0].Columns.Contains("ConferenceTypeDescription"))
                            ds.Tables[0].Columns.Add("ConferenceTypeDescription");

                        foreach (DataRow dr in dv.Table.Rows)
                        {
                            switch (dr["ConferenceType"].ToString())
                            {
                                case ns_MyVRMNet.vrmConfType.AudioOnly:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Audio-Only");
                                    break;
                                case ns_MyVRMNet.vrmConfType.AudioVideo:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Audio/Video");
                                    break;
                                case ns_MyVRMNet.vrmConfType.P2P:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Point to Point");
                                    break;
                                case ns_MyVRMNet.vrmConfType.RoomOnly:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Room Only");
                                    break;
                                case ns_MyVRMNet.vrmConfType.HotDesking:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Hotdesking");
                                    break;
                                default:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Undefined");
                                    break;
                            }
                            dr["ConferenceName"] = dr["ConferenceName"].ToString().Replace("{P}", "<sup><a style='font-size:medium; color:Red'>*</a></sup>");
                        }
                    }
                    dt = dv.Table;
                    dgList.DataSource = dt;
                    dgList.DataBind();

                    foreach (DataGridItem dgi in dgList.Items)
                    {
                        if (Request.QueryString["t"] != null && Request.QueryString["t"].ToString().Equals("1"))
                        {
                            //ZD 101233 Starts
                            int confStatus = 0, isPublicConf = 0;
                            int.TryParse(dgi.Cells[4].Text, out confStatus);
                            int.TryParse(dgi.Cells[11].Text, out isPublicConf);

                            int filterType = 0;
                            hasView = 0;
                            hasEdit = 0;
                            hasDelete = 0;
                            hasManage = 0;
                            hasClone = 0;
                            hasMCUInfo = 0;
                            hasExtendTime = 0;

                            if (confStatus == 0 && isPublicConf == 1)
                                filterType = 4;
                            else if (confStatus == 1)
                                filterType = 5;
                            else if (confStatus == 6)
                                filterType = 8;
                            else if (confStatus == 5)
                                filterType = 2;
                            else if (confStatus == 3 || confStatus == 9) //Terminated or Deleted
                                filterType = 9;
                            else if (confStatus == 7) //Completed
                                filterType = 10;
                            else
                                filterType = 3; //if (confStatus == 0 && isPublicConf == 0)//Reservation with Puclic or ConfSupport or without both

                            obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);

                            if (hasView == 0 && (LinkButton)dgi.FindControl("btnViewDetails") != null)
                                ((LinkButton)dgi.FindControl("btnViewDetails")).Visible = false;
                            if (hasManage == 0 && (LinkButton)dgi.FindControl("btnManage") != null)
                                ((LinkButton)dgi.FindControl("btnManage")).Visible = false;
                            if (hasExtendTime == 0 && (LinkButton)dgi.FindControl("btnExtendtime") != null)
                                ((LinkButton)dgi.FindControl("btnExtendtime")).Visible = false;
                            if (hasExtendTime == 0 && (LinkButton)dgi.FindControl("hasMCUInfo") != null)
                                ((LinkButton)dgi.FindControl("hasMCUInfo")).Visible = false;
                            if (hasEdit == 0 && (LinkButton)dgi.FindControl("btnEdit") != null)
                                ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                            if (hasClone == 0 && (LinkButton)dgi.FindControl("btnClone") != null)
                                ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                            if (hasDelete == 0 && (LinkButton)dgi.FindControl("btnDelete") != null)
                                ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;

                            if (hasEdit == 0)
                                ((CheckBox)dgi.FindControl("chkChangeMCU")).Visible = false;
                            //ZD 101233 End
                            switch (dgi.Cells[4].Text)
                            {
                                case ns_MyVRMNet.vrmConfStatus.Deleted:
                                    if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                        ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                    if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                        ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                    break;
                                case ns_MyVRMNet.vrmConfStatus.Terminated:
                                    if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                        ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                    if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                        ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                    if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                        ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                    break;
                                case ns_MyVRMNet.vrmConfStatus.OnMCU:
                                    if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                        ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                    //ZD 100036 Starts
                                    if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                    {
                                        if (dgi.Cells[10].Text.Equals("1") && hasDelete == 1) //If Synchronous conf-Edit and Delete = True //ZD 101233
                                            ((LinkButton)dgi.FindControl("btnDelete")).Visible = true;
                                        else
                                            ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                    }
                                    if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                    {
                                        if (dgi.Cells[10].Text.Equals("1") && hasEdit == 1) //ZD 101233
                                            ((LinkButton)dgi.FindControl("btnEdit")).Visible = true;
                                        else
                                            ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                    }
                                    //ZD 100036 End
                                    break;

                            }

                        }
                    }

                    lblNoConferences.Visible = false;
                    trPages.Visible = true;
                }
                else
                {
                    lblNoConferences.Visible = true;
                    trPages.Visible = false;
                }


                if (Session["admin"].ToString() != "2" && Session["admin"].ToString() != "3")
                    trPages.Visible = false;
            }
            catch (Exception ex)
            {
                log.Trace("conferenceList: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region SortGrid
        /// <summary>
        /// SortGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SortGrid(Object sender, CommandEventArgs e)
        {
            try
            {
                txtSortBy.Value = e.CommandArgument.ToString();
                if (Request.QueryString["t"].ToString().Equals("1"))
                    BindDataFromSearch();
                DisplaySort();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SortGrid:" + ex.Message);
            }
        }
        #endregion

        #region DisplaySort
        /// <summary>
        /// DisplaySort
        /// </summary>
        protected void DisplaySort()
        {
            DataGridItem dgi = (DataGridItem)dgConferenceList.Controls[0].Controls[0];
            switch (txtSortBy.Value)
            {
                case "1":
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "2":
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "3":
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "4":
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "5":
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:underline");
                    break;
            }

        }
        #endregion

        #region EditConference
        protected void EditConference(Object sender, DataGridCommandEventArgs e)
        {


            try
            {
                if (((LinkButton)e.CommandSource).Text.IndexOf(obj.GetTranslatedText("Delete") + " &") >= 0)
                {
                    DeleteConference(sender, e);
                }
                else if (((LinkButton)e.CommandSource).Text.IndexOf(obj.GetTranslatedText("Edit")) >= 0)
                {

                    string redirectPage = "ConferenceSetup.aspx?t=";
                    Session.Add("ConfID", e.Item.Cells[0].Text);

                    if (Session["isExpressUser"] != null)
                    {
                        if (Session["isExpressUser"].ToString() == "1")
                            redirectPage = "ExpressConference.aspx?t=";
                    }

                    Response.Redirect("~/" + language + "/" + redirectPage);
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("EditConference:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region DeleteConference
        protected void DeleteConference(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <delconference>";
                inXML += "      <conference>";
                inXML += "          <confID>" + e.Item.Cells[0].Text + "</confID>";
                inXML += "          <reason></reason>";
                inXML += "      </conference>";
                inXML += "  </delconference>";
                inXML += "</login>";
                string outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("DeleteConference - " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    switch (Request.QueryString["t"].ToString())
                    {
                        case "1":
                            BindDataFromSearch();
                            break;
                        default:
                            errLabel.Text = obj.GetTranslatedText("Illegal Search");
                            break;
                    }
                }
                else
                {
                    if (Application["External"].ToString() != "")
                    {
                        string inEXML = "";
                        inEXML = "<SetExternalScheduling>";
                        inEXML += "<confID>" + e.Item.Cells[0].Text + "</confID>";
                        inEXML += "</SetExternalScheduling>";

                        string outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                    }
                    string inxml = "<DeleteParticipantICAL>";
                    inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                    inxml += "<ConfID>" + e.Item.Cells[0].Text + "</ConfID>";
                    if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                        inxml += "<isExchange>1</isExchange>";
                    //ZD 100924 Start
                    else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                        inxml += "<isExchange>2</isExchange>";
                    else
                        inxml += "<isExchange>0</isExchange>";
                    //ZD 100924 End
                    inxml += "</DeleteParticipantICAL>";
                    obj.CallCommand("DeleteParticipantICAL", inxml);
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    inxml = "<DeleteCiscoICAL>";
                    inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                    inxml += "<ConfID>" + e.Item.Cells[0].Text + "</ConfID>";
                    inxml += "</DeleteCiscoICAL>";
                    obj.CallCommand("DeleteCiscoICAL", inxml);
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    Session["CalendarMonthly"] = null;
                    inXML = "";
                    inXML += "<login>";
                    inXML += obj.OrgXMLElement();
                    inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                    inXML += "  <ConferenceID>" + e.Item.Cells[0].Text + "</ConferenceID>";
                    inXML += "  <WorkorderID>0</WorkorderID>";
                    inXML += "</login>";
                    outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        if (((LinkButton)e.CommandSource).Text.IndexOf(obj.GetTranslatedText("Delete") + " &") >= 0)
                        {
                            CloneConference(sender, e);
                        }
                        else
                        {
                            string qString = "m=1&t=" + Request.QueryString["t"].ToString();
                            if (Request.QueryString["pageNo"] != null)
                                qString += "&pageNo=" + Request.QueryString["pageNo"].ToString();
                            Response.Redirect("ConferenceList.aspx?" + qString);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteConference:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ManageConference
        protected void ManageConference(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument.ToString().Equals("1"))
                {
                    Session.Add("ConfID", e.Item.Cells[0].Text);
                    Response.Redirect("ManageConference.aspx?t=");
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ManageConference:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region CloneConference
        protected void CloneConference(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("ConfID", e.Item.Cells[0].Text);
                Response.Redirect("ConferenceSetup.aspx?t=o");
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CloneConference:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        /// <summary>
        /// BindRowsDeleteMessage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    if (btnTemp != null)
                    {
                        if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                            btnTemp.Attributes.Add("onclick", "if (confirm('Are you sure you want to delete this hearing?')) return true; else {DataLoading('0'); return false;}");
                        else
                            btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this conference?") + "')) return true; else {DataLoading('0'); return false;}");
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ShowHide
        /// <summary>
        /// ShowHide
        /// </summary>
        protected void ShowHide()
        {
            try
            {
                DataGridItem dgi = (DataGridItem)dgConferenceList.Controls[0].Controls[0];
                if (hdnSortingOrder.Value == "1" && hdnAscendingOrder.Value == "1")
                {
                    ((Label)dgi.FindControl("spnAscendingID")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfName")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingSiloName")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfDate")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingIDIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfDateIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingSiloNameIE")).Visible = false;

                    ((Label)dgi.FindControl("spnDescendingID")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingConfName")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingSiloName")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingConfDate")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingIDIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingConfNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingConfDateIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingSiloNameIE")).Visible = true;
                    hdnAscendingOrder.Value = "0";
                }
                else
                {
                    ((Label)dgi.FindControl("spnAscendingID")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfName")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingSiloName")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfDate")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingIDIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfDateIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingSiloNameIE")).Visible = true;

                    ((Label)dgi.FindControl("spnDescendingID")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingConfName")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingSiloName")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingConfDate")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingIDIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingConfNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingConfDateIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingSiloNameIE")).Visible = false;
                    hdnAscendingOrder.Value = "1";
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ShowHide:" + ex.Message);
            }
        }
        #endregion

        #region BindData
        /// <summary>
        /// BindData
        /// </summary>
        /// <returns></returns>
        private bool BindData()
        {
            string inXML = "", outXML = "";

            try
            {
                inXML = "<login>" + obj.OrgXMLElement() + " <userID>" + Session["userID"].ToString() + "</userID></login>";
                outXML = obj.CallMyVRMServer("GetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                    if (nodes.Count > 0)
                    {
                        XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeTypes/type");
                        LoadList(lstBridgeType, nodesTemp);
                        nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeStatuses/status");
                        LoadList(lstBridgeStatus, nodesTemp);
                        LoadMCUGrid(nodes);

                        dgMCUs.Visible = true;
                        tblNoMCUs.Visible = false;
                    }
                    else
                    {
                        dgMCUs.Visible = false;
                        tblNoMCUs.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowSystemMessage();
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);
            }
            return true;
        }
        #endregion

        #region LoadList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstTemp"></param>
        /// <param name="nodes"></param>
        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadList:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region LoadMCUGrid
        /// <summary>
        /// LoadMCUGrid
        /// </summary>
        /// <param name="nodes"></param>
        protected void LoadMCUGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }

                foreach (DataRow dr in dt.Rows)
                {

                    if (dr["exist"].ToString().Equals("1"))
                        dr["exist"] = obj.GetTranslatedText("Existing");
                    else
                        dr["exist"] = obj.GetTranslatedText("Virtual");
                    dr["status"] = obj.GetTranslatedText(lstBridgeStatus.Items.FindByValue(dr["status"].ToString()).Text);
                    dr["interfaceType"] = lstBridgeType.Items.FindByValue(dr["interfaceType"].ToString()).Text;

                    if (dr["PollStatus"].ToString() == "-2")
                    {
                        dr["PollStatus"] = "<font color=#FF0000> " + obj.GetTranslatedText("FAIL") + " </font>";
                    }
                    else if (dr["PollStatus"].ToString() == "-1")
                    {
                        dr["PollStatus"] = "<font color=#F5B800> " + obj.GetTranslatedText("PASS") + " </font>";
                    }
                    else if (dr["PollStatus"].ToString() == "0")
                    {
                        dr["PollStatus"] = "<font color=#008000> " + obj.GetTranslatedText("PASS") + " </font>";
                    }

                    if (dr["userstate"].ToString() == "I")
                    {
                        dr["administrator"] = "<font color=red>" + dr["administrator"].ToString() + " (In-Active)</font>";
                    }
                    else if (dr["userstate"].ToString() == "D")
                    {
                        dr["administrator"] = "<font color=red>" + dr["administrator"].ToString() + "</font>";
                    }
                }

                dgMCUs.DataSource = dt;
                dgMCUs.DataBind();

                if (Session["SelectedMCUId"] != null)
                {
                    foreach (DataGridItem dgMCUItem in dgMCUs.Items)
                    {
                        if (dgMCUItem.Cells[0].Text == Session["SelectedMCUId"].ToString())
                        {
                            ((RadioButton)dgMCUItem.FindControl("rdSelectMCU")).Checked = false;
                            ((RadioButton)dgMCUItem.FindControl("rdSelectMCU")).Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadMCUGrid:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region SubmitMCUSelection
        /// <summary>
        /// SubmitMCUSelection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitMCUSelection(Object sender, EventArgs e)
        {
            string searchXML = "", MCUId = "", MCUName = "";
            int ChangedMCU = 0;
            StringBuilder inXML = null;
            XmlDocument xd = null;
            int checkAllPages = 0, checkSelectPage = 0;
            Label confUID = new Label();
            string outXML = "";
            try
            {
                if (Session["ChangeinXML"] == null)
                    return;

                if (chkAllPages.Checked)
                    checkAllPages = 1;

                if (chkSelectPage.Checked)
                    checkSelectPage = 1;

                xd = new XmlDocument();
                searchXML = Session["ChangeinXML"].ToString();
                xd.LoadXml(searchXML);

                if (xd.SelectSingleNode("//SearchConference/SelectedMCU/ID") != null)
                    int.TryParse(xd.SelectSingleNode("//SearchConference/SelectedMCU/ID").InnerText.Trim(), out ChangedMCU);

                foreach (DataGridItem dgMCUItem in dgMCUs.Items)
                {
                    if (((RadioButton)dgMCUItem.FindControl("rdSelectMCU")).Checked == true)
                    {
                        MCUId = dgMCUItem.Cells[0].Text;
                        MCUName = dgMCUItem.Cells[1].Text;
                    }
                }
                inXML = new StringBuilder();
                inXML.Append("<ChangeMCU>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<MCU><ID>" + MCUId + "</ID></MCU>");                    //New MCU ID
                inXML.Append("<ChangedMCU><ID>" + ChangedMCU + "</ID></ChangedMCU>"); //Old MCU ID

                inXML.Append("<Conferences>");
                if (checkAllPages <= 0)
                {
                    foreach (DataGridItem dgConfItem in dgConferenceList.Items)
                    {
                        if (((CheckBox)dgConfItem.FindControl("chkChangeMCU")).Checked == true)
                        {
                            confUID = (Label)dgConfItem.FindControl("lblUniqueID");
                            if (confUID != null)
                                inXML.Append("<Conference><confUID>" + confUID.Text + "</confUID></Conference>");
                        }
                    }
                }

                inXML.Append("<SearchCriteria>");
                inXML.Append(Session["ChangeinXML"].ToString()); //SearchConferene inXML
                inXML.Append("</SearchCriteria>");
                inXML.Append("<All> " + checkAllPages + "</All>");
                inXML.Append("</Conferences>");
                inXML.Append("</ChangeMCU>");

                outXML = obj.CallMyVRMServer("ChangeConfMCU", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<errro>") < 0)
                    BindDataFromSearch();
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = "Error in Changing MCU for selected conference(s)";
                    log.Trace("outXML" + outXML);
                }

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SubmitMCUSelection:" + ex.Message);
            }
        }
        #endregion

        #region btnCancel_Click
        /// <summary>
        /// btnCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("SearchConferenceInputParameters.aspx");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("btnCancel_Click" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

    }

}