﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100866 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Xml;

public partial class Droid_conference : System.Web.UI.Page
{
    StringBuilder inXML = new StringBuilder();
    protected String confInXML = null;
    protected String setAdvAVSetInXML = null;
    myVRMNet.NETFunctions obj = null;
    protected String xmlstr = null;
    protected String RoomOutXML = null;
    protected String rooms = null;
    public string DefaultConferenceType;

    public Droid_conference()
    {
        obj = new myVRMNet.NETFunctions();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["username"] != null)
            txtReqName.Text = Session["username"].ToString();
        else
            Response.Redirect("login.aspx");

        if (!IsPostBack)
        {

            //ZD 101030
            if (Session["EnableImmConf"].ToString() == "0")
            {
                StartNowRow.Visible = false;
                trDuration.Visible = false;
            }
            else
            {
                if (Session["DefaultConfDuration"] != null)
                {
                    int ConfDuration = Convert.ToInt32(Session["DefaultConfDuration"]);
                    int ConfHours = ConfDuration / 60;
                    String hrs = (ConfHours < 10) ? "0" + ConfHours.ToString() : ConfHours.ToString();
                    int ConfMinutes = ConfDuration % 60;
                    String Mins = (ConfMinutes < 10) ? "0" + ConfMinutes.ToString() : ConfMinutes.ToString();
                    lstDuration.Text = hrs + ":" + Mins;
                }
            }

            if (Session["DefaultConferenceType"] != null)
            {
                lstConferenceType.ClearSelection();//FB 2377
                DefaultConferenceType = Session["DefaultConferenceType"].ToString();
                if (DefaultConferenceType != "")
                    lstConferenceType.Items.FindByValue(DefaultConferenceType).Selected = true;
            }            

            if (Session["P2PEnable"] != null && Session["P2PEnable"].ToString().Equals("0"))
                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("4"));
            if (Session["EnableRoomConfType"] != null && Session["EnableRoomConfType"].ToString().Equals("0"))
                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("7"));
            if (Session["EnableAudioVideoConfType"] != null && Session["EnableAudioVideoConfType"].ToString().Equals("0"))
                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("2"));
            if (Session["EnableAudioOnlyConfType"] != null && Session["EnableAudioOnlyConfType"].ToString().Equals("0"))
                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("6"));
            if (Session["EnableHotdeskingConference"] != null && Session["EnableHotdeskingConference"].ToString().Equals("0"))
                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("8"));


            if(lstConferenceType.SelectedValue == "8")
            {
                trInviteParty.Attributes.Add("style","display:none");
                confTitle.Value = "HotDesking Reservation";
                trConfTitle.Attributes.Add("style","display:none");
                if(tblStartNowRow != null)
                    tblStartNowRow.Attributes.Add("style","display:none");
                trConfDisc.Attributes.Add("style","display:none");
                hdnConferenceName.Value = "true";
                SpnRoomtabNo.InnerText = "2.";
                SpnSplInstrNo.InnerText = "3.";
                SpnReviewNo.InnerText = "4.";
            }
            else
            {
                trInviteParty.Attributes.Add("style","display:");
                if(confTitle.Value == "HotDesking Reservation" && hdnConferenceName.Value == "true")
                    confTitle.Value = "";
                trConfTitle.Attributes.Add("style","display:");
                if (tblStartNowRow != null)
                    tblStartNowRow.Attributes.Add("style","display:");
                trConfDisc.Attributes.Add("style","display:");
                hdnConferenceName.Value = "true";
                SpnRoomtabNo.InnerText = "3.";
                SpnSplInstrNo.InnerText = "4.";
                SpnReviewNo.InnerText = "5.";
            }
            

            BuildParticipantsXML();
            BindParticipants();

            DateTime dateStart = new DateTime();
            DateTime sysDate = DateTime.Now;

            if (Request.QueryString["sd"] != null)
            {
                string sdVal = Request.QueryString["sd"].ToString();
                string stVal = Request.QueryString["st"].ToString();
                dateStart = DateTime.Parse(sdVal + " " +stVal);
                int result = DateTime.Compare(dateStart, sysDate);
                if (result >= 0)
                    sysDate = dateStart;
            }


            //calStart.SelectedDate = DateTime.Now.AddHours(1);
            //--calStart.SelectedDate = sysDate.AddHours(1);
            //DateTime dt = DateTime.Now.AddHours(1);
            DateTime dt = sysDate.AddHours(1);
            txtConfStart.Text = String.Format("{0:MM/dd/yyyy}", dt);
            recConfStart.Text = String.Format("{0:MM/dd/yyyy}", dt);
            DateTime dt2 = DateTime.Now;
            curDate.Value = String.Format("{0:MM/dd/yyyy}", dt2);
            //--calEnd.SelectedDate = sysDate.AddHours(2);
            dt = dt.AddHours(1);
            txtConfEnd.Text = String.Format("{0:MM/dd/yyyy}", dt);
            //int h = DateTime.Now.Hour + 1;
            int h = sysDate.Hour + 1;
            if (h > 12)
                h = h - 12;
            string noon = DateTime.Now.AddHours(1).ToString();
            noon = noon.Substring(noon.Length - 2, 2);
            string time = string.Empty;
            if (h < 10)
            {
                time = "0" + h + ":00 " + noon;
            }
            else
            {
                time = h.ToString() + ":00 " + noon;
            }
            hdnStartTime.Value = time;
            h = DateTime.Now.Hour + 2;
            if (h > 12)
                h = h - 12;
            noon = DateTime.Now.AddHours(2).ToString();
            noon = noon.Substring(noon.Length - 2, 2);
            time = string.Empty;
            if (h < 10)
            {
                time = "0" + h + ":00 " + noon;
            }
            else
            {
                time = h.ToString() + ":00 " + noon;
            }
            hdnEndTime.Value = time;
        
        }

        if (Session["recurEnable"].ToString() == "0")
        {
            recurTr.Visible = false;
            xRecurTr.Visible = false;
            xRecurTextTr.Visible = false;
        }

    }
        
    protected void setConference()
    {
        try
        {
            DataTable dtRooms = new DataTable();
            int maxDuration = 24;
            if (Application["MaxConferenceDurationInHours"] != null)
                if (!Application["MaxConferenceDurationInHours"].ToString().Trim().Equals(""))
                    maxDuration = Int32.Parse(Application["MaxConferenceDurationInHours"].ToString().Trim());

            string confInXML = "";
            confInXML += "<conference>";
            confInXML += obj.OrgXMLElement();//"<organizationID>11</organizationID>"; // 
            confInXML += "<userID>" + Session["userID"].ToString() + "</userID>";
            confInXML += "<confInfo>";
            confInXML += "<confID>new</confID>";
            confInXML += "<confName>" + confTitle.Value.ToString() + "</confName>";
            confInXML += "<confHost>" + Session["userID"].ToString() + "</confHost>";
            confInXML += "<confOrigin>0</confOrigin>";
            confInXML += "<timeCheck></timeCheck>";
            confInXML += "<confPassword></confPassword>";
            //confInXML += "<immediate>0</immediate>";

            if (recurStatus.Value == "No")
            {
                //ZD 101030
                int durationMin;
                if (chkStartNow.Checked)
                {
                    confInXML += "      <immediate>1</immediate>";
                    confInXML += "      <recurring>0</recurring>";
                    confInXML += "      <recurringText></recurringText>";
                    confInXML += "		<startDate></startDate>";
                    confInXML += "		<startHour></startHour>";
                    confInXML += "		<startMin></startMin>";
                    confInXML += "		<startSet></startSet>";
                    confInXML += "		<timeZone>" + drpTimeZone.Value.ToString() + "</timeZone>";
                    confInXML += "		<setupDuration>0</setupDuration>";
                    confInXML += "		<teardownDuration>0</teardownDuration>";
                    confInXML += "		<setupDateTime></setupDateTime>";
                    confInXML += "		<teardownDateTime></teardownDateTime>";
                    durationMin = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);
                    confInXML += "<durationMin>" + durationMin.ToString() + "</durationMin>";
                }
                else
                {

                    confInXML += "<recurring>0</recurring>";
                    confInXML += "<recurringText></recurringText>";
                    confInXML += "<startDate>" + txtConfStart.Text.ToString() + "</startDate>";
                    string startTime = hdnStartTime.Value.ToString();
                    confInXML += "<startHour>" + startTime.Substring(0, 2).ToString() + "</startHour>";
                    confInXML += "<startMin>" + startTime.Substring(3, 2).ToString() + "</startMin>";
                    confInXML += "<startSet>" + startTime.Substring(6, 2).ToString() + "</startSet>";
                    confInXML += "<timeZone>" + drpTimeZone.Value.ToString() + "</timeZone>";
                    string setupDateTime = txtConfStart.Text.ToString() + " " + hdnStartTime.Value.ToString();
                    string teardownDateTime = txtConfEnd.Text.ToString() + " " + hdnEndTime.Value.ToString();
                    setupDateTime = setupDateTime.Insert(16, ":00");
                    teardownDateTime = teardownDateTime.Insert(16, ":00");
                    DateTime sdate = Convert.ToDateTime(setupDateTime);
                    DateTime edate = Convert.ToDateTime(teardownDateTime);
                    TimeSpan ts = edate - sdate;
                    confInXML += "<setupDuration></setupDuration>";
                    confInXML += "<teardownDuration></teardownDuration>";
                    confInXML += "<setupDateTime>" + setupDateTime.ToString() + "</setupDateTime>";
                    confInXML += "<teardownDateTime>" + teardownDateTime.ToString() + "</teardownDateTime>";
                    confInXML += "<durationMin>" + ((ts.Hours * 60) + ts.Minutes).ToString() + "</durationMin>";
                }
            }
            else
            {

                confInXML += "<setupDateTime></setupDateTime>";
                confInXML += "<teardownDateTime></teardownDateTime>";

                string recurrance = getRecurrance();
                confInXML += recurrance;
            }


            //confInXML += "<createBy>2</createBy>"; //ZD 101141
            confInXML += "<createBy>" + lstConferenceType.SelectedValue.ToString() +"</createBy>";

            bool isError = false;
            string xConfDisc = (confDisc.Value.ToString().Trim() == "Conference Description") ? "" : confDisc.Value.ToString();
            confInXML += "<description>" + xConfDisc + "</description>";
            string selecRooms = selectedRoom(ref isError, ref dtRooms);
            if (isError == true)
            {
                errMsg.Visible = true;
                return;
            }

            confInXML += selecRooms;            

            confInXML += "<publicConf>0</publicConf>";
            confInXML += "<dynamicInvite>0</dynamicInvite>";
            confInXML += "<advAVParam>";
            confInXML += "<maxAudioPart>0</maxAudioPart>";
            confInXML += "<maxVideoPart>0</maxVideoPart>";
            confInXML += "<restrictProtocol>3</restrictProtocol>";
            confInXML += "<restrictAV>2</restrictAV>";
            confInXML += "<videoLayout>01</videoLayout>";
            confInXML += "<maxLineRateID>384</maxLineRateID>";
            confInXML += "<audioCodec>0</audioCodec>";
            confInXML += "<videoCodec>0</videoCodec>";
            confInXML += "<dualStream>1</dualStream>";
            confInXML += "<confOnPort>0</confOnPort>";
            confInXML += "<encryption>0</encryption>";
            confInXML += "<lectureMode>0</lectureMode>";
            confInXML += "<VideoMode>3</VideoMode>";
            confInXML += "<SingleDialin>0</SingleDialin>";
            confInXML += "</advAVParam>";
            string selecPart = selectedParticipant();
            confInXML += selecPart;
            confInXML += "<ModifyType>0</ModifyType>";
            confInXML += "<fileUpload>";
            confInXML += "<file></file>";
            confInXML += "<file></file>";
            confInXML += "<file></file>";
            confInXML += "</fileUpload>";
            confInXML += "<CustomAttributesList>";
            confInXML += "<CustomAttribute>";
            confInXML += "<CustomAttributeID>1</CustomAttributeID>";
            confInXML += "<OptionID>-1</OptionID>";
            confInXML += "<Type>4</Type>";
            confInXML += "<OptionValue></OptionValue>";
            confInXML += "</CustomAttribute>";
            confInXML += "<CustomAttribute>";
            confInXML += "<CustomAttributeID>2</CustomAttributeID>";
            confInXML += "<OptionID>-1</OptionID>";
            confInXML += "<Type>4</Type>";
            confInXML += "<OptionValue></OptionValue>";
            confInXML += "</CustomAttribute>";
            confInXML += "<CustomAttribute>";
            confInXML += "<CustomAttributeID>3</CustomAttributeID>";
            confInXML += "<OptionID>-1</OptionID>";
            confInXML += "<Type>10</Type>";
            string xSplInst = (splInst.Value.ToString().Trim() == "Type instruction here.") ? "" : splInst.Value.ToString();
            confInXML += "<OptionValue>" + xSplInst + "</OptionValue>";
            confInXML += "</CustomAttribute>";
            confInXML += "</CustomAttributesList>";
            confInXML += "<ICALAttachment />";
            confInXML += "</confInfo>";
            confInXML += "</conference>";
            String outxml = obj.CallCommand("SetConference", confInXML);
            XDocument xd = XDocument.Parse(outxml);
            if (outxml.IndexOf("<error>") > -1)
            {
                var xlv1s = (from xlv1 in xd.Descendants("error")
                            select xlv1.Element("message").Value).ToList();
                errMsg.Text = xlv1s[0].ToString();
                return;
            }
            /*
            if (rooms == null)
            {
                //Response.Redirect("calendar.aspx");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "confAlert", "<script>fnShowConfAlert();</script>", false);
                //ScriptManager.RegisterStartupScript
            }
            */
            var lv1s = (from lv1 in xd.Descendants("conference")
                        select lv1.Element("confID").Value).ToList();

            if (lstConferenceType.SelectedValue.Equals("7"))
            {
                setAdvAVSetInXML = "";
                setAdvAVSetInXML += "<SetAdvancedAVSettings>";
                setAdvAVSetInXML += "<editFromWeb>1</editFromWeb>";//FB 2235
                setAdvAVSetInXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                setAdvAVSetInXML += "  <ConfID>" + lv1s[0].ToString() + "</ConfID>";
                setAdvAVSetInXML += obj.OrgXMLElement();
                setAdvAVSetInXML += "  <AVParams>";
                setAdvAVSetInXML += "<SingleDialin>0</SingleDialin>";
                setAdvAVSetInXML += "  </AVParams>";
                setAdvAVSetInXML += "  <Endpoints>";
                setAdvAVSetInXML += "  </Endpoints>";
                setAdvAVSetInXML += "</SetAdvancedAVSettings>";
            }
            else
            {
                setAdvAVSetInXML += "<SetAdvancedAVSettings>";
                setAdvAVSetInXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                setAdvAVSetInXML += "<ConfID>" + lv1s[0].ToString() + "</ConfID>";
                setAdvAVSetInXML += obj.OrgXMLElement();
                setAdvAVSetInXML += "<AVParams>";
                setAdvAVSetInXML += "<SingleDialin>0</SingleDialin>";
                setAdvAVSetInXML += "</AVParams>";
                setAdvAVSetInXML += "<Endpoints>";
                if (dtRooms != null)
                {
                    //string[] roomCount = rooms.Split(',');
                    //for (int i = 0; i < roomCount.Length - 1; i++)
                    for (int i = 0; i < dtRooms.Rows.Count; i++)                        
                    {
                        setAdvAVSetInXML += "<Endpoint>";
                        setAdvAVSetInXML += "<Type>R</Type>";
                        setAdvAVSetInXML += "<ID>" + dtRooms.Rows[i]["ID"].ToString().Trim() + "</ID>";
                        setAdvAVSetInXML += "<UseDefault>1</UseDefault>";
                        setAdvAVSetInXML += "<IsLecturer>0</IsLecturer>";                        
                        setAdvAVSetInXML += "<EndpointID>" + dtRooms.Rows[i]["EndpointID"].ToString().Trim() + "</EndpointID>";                        
                        setAdvAVSetInXML += "<ProfileID>" + dtRooms.Rows[i]["ProfileID"].ToString().Trim() + "</ProfileID>";
                        setAdvAVSetInXML += "<BridgeID>" + dtRooms.Rows[i]["BridgeID"].ToString().Trim() + "</BridgeID>";
                        setAdvAVSetInXML += "<AddressType></AddressType>";
                        setAdvAVSetInXML += "<Address></Address>";
                        setAdvAVSetInXML += "<VideoEquipment></VideoEquipment>";
                        setAdvAVSetInXML += "<connectionType />";
                        setAdvAVSetInXML += "<Bandwidth></Bandwidth>";
                        setAdvAVSetInXML += "<IsOutside></IsOutside>";
                        setAdvAVSetInXML += "<DefaultProtocol></DefaultProtocol>";
                        setAdvAVSetInXML += "<Connection>2</Connection>";
                        setAdvAVSetInXML += "<URL />";
                        setAdvAVSetInXML += "<ExchangeID />";
                        setAdvAVSetInXML += "<APIPortNo>23</APIPortNo>";
                        if (lstConferenceType.SelectedValue.Equals("4"))
                            setAdvAVSetInXML += "<Connect2>1</Connect2>";
                        else
                            setAdvAVSetInXML += "<Connect2>-1</Connect2>";
                        setAdvAVSetInXML += "</Endpoint>";
                    }
                }
                setAdvAVSetInXML += "</Endpoints>";
                setAdvAVSetInXML += "</SetAdvancedAVSettings>";
            }
            String xOutXML = obj.CallMyVRMServer("SetAdvancedAVSettings", setAdvAVSetInXML, Application["MyVRMServer_ConfigPath"].ToString());
            //Response.Redirect("calendar.aspx");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "confMsg", "<script>fnShowConfAlert();</script>", false);
            //ScriptManager.RegisterStartupScript
        }
        catch (Exception ex)
        {
            //log.Trace("Error occurred in SetConference. Please try later.\n" + ex.Message);
            //errLabel.Visible = true;
            //return false;
        }

    }


    protected string getRecurrance()
    {
        string recur="";

        recur += "<recurring>1</recurring>";




        recur += "<appointmentTime>";
        recur += "<timeZone>" + drpTimeZone.Value.ToString() + "</timeZone>";
        string startTime = hdnRecurStart.Value.ToString();
        recur += "<startHour>" + startTime.Substring(0, 2).ToString() + "</startHour>";
        recur += "<startMin>" + startTime.Substring(3, 2).ToString() + "</startMin>";
        recur += "<startSet>" + startTime.Substring(6, 2).ToString() + "</startSet>";
        recur += "<durationMin>" + (( Int32.Parse(txtHrs.Value) * 60) + Int32.Parse(txtMins.Value)).ToString() + "</durationMin>";
        recur += "<setupDuration></setupDuration>";
        recur += "<teardownDuration></teardownDuration>";
        recur += "</appointmentTime>";


        //string recurText = "Occurs ";

        recur += "<recurrencePattern>";

        if (recurType.Value == "divDaily")
        {
            recur += "<recurType>1</recurType>";
            if (dailyType1.Checked == true)
            {
                recur += "<dailyType>1</dailyType>";
                recur += "<dayGap>" + txtDailyType.Value.ToString() + "</dayGap>";
                //recurText += "every " + txtDailyType.Value.ToString() + " day(s)";
            }
            else
            {
                recur += "<dailyType>2</dailyType>";
                recur += "<dayGap>-1</dayGap>";
                //recurText += "every weekday";
            }
        }

        if (recurType.Value == "divWeekly")
        {
            recur += "<recurType>2</recurType>";
            recur += "<weekGap>" + txtWkGap.Value.ToString() + "</weekGap>";
            //recurText += "every " + txtWkGap.Value.ToString() + " week(s) on ";


            string wkDay = "";
            if(chkSunId.Checked == true)
            {
                wkDay += chkSunId.Value;
                //recurText += "Sunday ,";
            }
            if (chkMonId.Checked == true)
            {
                if(wkDay == "")
                    wkDay += chkMonId.Value;
                else
                    wkDay += "," + chkMonId.Value;
                //recurText += "Monday ,";
            }
            if (chkTueId.Checked == true)
            {
                if (wkDay == "")
                    wkDay += chkTueId.Value;
                else
                    wkDay += "," + chkTueId.Value;
                //recurText += "Tuesday ,";
            }
            if (chkWedId.Checked == true)
            {
                if (wkDay == "")
                    wkDay += chkWedId.Value;
                else
                    wkDay += "," + chkWedId.Value;
                //recurText += "Wednesday ,";
            }
            if (chkThuId.Checked == true)
            {
                if (wkDay == "")
                    wkDay += chkThuId.Value;
                else
                    wkDay += "," + chkThuId.Value;
                //recurText += "Thursday ,";
            }
            if (chkFriId.Checked == true)
            {
                if (wkDay == "")
                    wkDay += chkFriId.Value;
                else
                    wkDay += "," + chkFriId.Value;
                //recurText += "Friday ,";
            }
            if (chkSatId.Checked == true)
            {
                if (wkDay == "")
                    wkDay += chkSatId.Value;
                else
                    wkDay += "," + chkSatId.Value;
                //recurText += "Saturday ,";
            }

            recur += "<weekDay>" + wkDay + "</weekDay>";


        }

        if (recurType.Value == "divMonthly")
        {
            recur += "<recurType>3</recurType>";
            if (monthlyType1.Checked == true)
            {
                recur += "<monthlyType>1</monthlyType>";
                recur += "<monthDayNo>" + drpSelDay1.Value.ToString() + "</monthDayNo>";
                //recurText += "day " + drpSelDay1.Items[drpSelDay1.SelectedIndex].Text.ToString() + " of every ";
                recur += "<monthGap>" + drpSelMon1.Value.ToString() + "</monthGap>";
                //recurText += drpSelMon1.Items[drpSelMon1.SelectedIndex].Text.ToString() + " month(s)";
            }
            else
            {
                recur += "<monthlyType>2</monthlyType>";
                recur += "<monthWeekDayNo>" + drpSelPos1.Value.ToString() + "</monthWeekDayNo>";
                //recurText += "the " + drpSelPos1.Items[drpSelPos1.SelectedIndex].Text.ToString() + " ";
                recur += "<monthWeekDay>" + drpSelWeek1.Value.ToString() + "</monthWeekDay>";
                //recurText += drpSelWeek1.Items[drpSelWeek1.SelectedIndex].Text.ToString() + " of every ";
                recur += "<monthGap>" + drpSelMonth1.Value.ToString() + "</monthGap>";
                //recurText += drpSelMonth1.Items[drpSelMonth1.SelectedIndex].Text.ToString();

            }

        }

        if (recurType.Value == "divYearly")
        {
            recur += "<recurType>4</recurType>";
            if (yearlyType1.Checked == true)
            {
                recur += "<yearlyType>1</yearlyType>";
                recur += "<yearMonth>" + drpSelMon2.Value.ToString() + "</yearMonth>";
                //recurText += "every " + drpSelMon2.Items[drpSelMon2.SelectedIndex].Text.ToString() + " ";
                recur += "<yearMonthDay>" + drpSelDay2.Value.ToString() + "</yearMonthDay>";
                //recurText += drpSelDay2.Items[drpSelDay2.SelectedIndex].Text.ToString();
            }
            else
            {
                recur += "<yearlyType>2</yearlyType>";
                recur += "<yearMonthWeekDayNo>" + drpSelPos2.Value.ToString() + "</yearMonthWeekDayNo>";
                //recurText += "the " + drpSelPos2.Items[drpSelPos2.SelectedIndex].Text.ToString() + " ";
                recur += "<yearMonthWeekDay>" + drpSelWeek2.Value.ToString() + "</yearMonthWeekDay>";
                //recurText += drpSelWeek2.Items[drpSelWeek2.SelectedIndex].Text.ToString() + " of ";
                recur += "<yearMonth>" + drpSelMonth2.Value.ToString() + "</yearMonth>";
                //recurText += drpSelMonth2.Items[drpSelMonth2.SelectedIndex].Text.ToString();

            }
        }

        if (recurType.Value == "divCustom")
        {
            //recurText = "Custom Date Selection: ";

            recur += "<recurType>5</recurType>";

            recur += "<startDates>";
            //int n = f_selection.Size;
            string[] dates = customDates.Value.Split(',');
            for (int i = 0; i < dates.Length-1; i++)
            {
                recur += "<startDate>" + dates[i] + "</startDate>";
                //if(i == 0)
                //    recurText += dates[i];
                //else
                //    recurText += ", " + dates[i];

            }
            recur += "</startDates>";


        }
        else
        {
            recur += "<recurrenceRange>";

            recur += "<startDate>" + recConfStart.Text.ToString() + "</startDate>";
            //recurText += " effective " + recConfStart.Text.ToString();
            recur += "<endType>" + recurRange.Value.ToString() + "</endType>";

            if(recurRange.Value.ToString() == "1")
                recur += "<occurrence>-1</occurrence>";
            else if (recurRange.Value.ToString() == "2")
            {
                recur += "<occurrence>" + txtEndAft.Value.ToString() + "</occurrence>";
                //recurText += " until " + txtEndAft.Value.ToString();
            }
            else if (recurRange.Value.ToString() == "3")
            {
                recur += "<endDate>" + recConfEnd.Text.ToString() + "</endDate>";
                //recurText += " occurs " + recConfEnd.Text.ToString() + " time(s)";
            }

            recur += "</recurrenceRange>";

        }
        //recurText += " from " + drpRecurStart.Value.ToString() + " for " + txtHrs.Value.ToString() + " hr(s) " + txtMins.Value.ToString() + " min(s)" ;

        recur += "</recurrencePattern>";

        recur += "<recurringText>" + hdnRecurText.Value.ToString() + "</recurringText>";

        return recur;
    }
    
    public class Endpoints
    {
        public string xProfileID { get; set; }
        public string xBridge { get; set; }

    }

    protected string selectedParticipant()
    {
        string selPatr = null;
        selPatr += "<partys>";
        for (int i = 0; i < dgParticipants.Items.Count; i++)
        {
            CheckBox isSelected = (System.Web.UI.WebControls.CheckBox) dgParticipants.Items[i].FindControl("chkSelectItem");
            if (isSelected.Checked == true)
            {
                selPatr += "<party>";
                selPatr += "<partyID>" + dgParticipants.Items[i].Cells[0].Text.ToString() + "</partyID>";
                selPatr += "<partyFirstName>" + dgParticipants.Items[i].Cells[1].Text.ToString() + "</partyFirstName>"; //FB 1640
                selPatr += "<partyLastName>" + dgParticipants.Items[i].Cells[2].Text.ToString() + "</partyLastName>";
                selPatr += "<partyEmail>" + dgParticipants.Items[i].Cells[3].Text.ToString() + "</partyEmail>";
                selPatr += "<partyInvite>2</partyInvite>";
                selPatr += "<partyNotify>1</partyNotify>";

                //ZD 101141
                //selPatr += "<partyAudVid>2</partyAudVid>";
                if(!lstConferenceType.SelectedValue.Equals("6"))
                    selPatr += "<partyAudVid>2</partyAudVid>";
                else
                    selPatr += "<partyAudVid>1</partyAudVid>";

                selPatr += "<notifyOnEdit>1</notifyOnEdit>"; //FB 1830 Email Edit
                selPatr += "</party>";
            }
        }

        string guestDetails = hdnGuestUsers.Value.ToString();
        string[] guests = guestDetails.Split('~');
        string[] detail;
        int k = guests.Count();
        for (int j = 0; j < k - 1; j++)
        {
            detail = guests[j].Split('`');
            selPatr += "<party>";
            selPatr += "<partyID>new</partyID>";
            selPatr += "<partyFirstName>" + detail[0].ToString() + "</partyFirstName>"; //FB 1640
            selPatr += "<partyLastName>" + detail[1].ToString() + "</partyLastName>";
            selPatr += "<partyEmail>" + detail[2].ToString() + "</partyEmail>";
            selPatr += "<partyInvite>2</partyInvite>";
            selPatr += "<partyNotify>1</partyNotify>";

            //ZD 101141
            //selPatr += "<partyAudVid>2</partyAudVid>";
            if (!lstConferenceType.SelectedValue.Equals("6"))
                selPatr += "<partyAudVid>2</partyAudVid>";
            else
                selPatr += "<partyAudVid>1</partyAudVid>";

            selPatr += "<notifyOnEdit>1</notifyOnEdit>"; //FB 1830 Email Edit
            selPatr += "</party>";
        }

        selPatr += "</partys>";
        return selPatr;

    }

    protected string selectedRoom(ref bool isError, ref DataTable dtRooms)
    {
        string RoomID = "";
        string selRoom = null;
             
        selRoom += "<locationList><selected>";
        for (int i = 0; i < dgRooms.Items.Count; i++)
        {
            CheckBox isSelected = (System.Web.UI.WebControls.CheckBox)dgRooms.Items[i].FindControl("chkSelectItem");
            if (isSelected.Checked == true)
            {
                RoomID = dgRooms.Items[i].Cells[0].Text.ToString();
                selRoom += "<level1ID>" + dgRooms.Items[i].Cells[0].Text.ToString() + "</level1ID>";
                rooms += dgRooms.Items[i].Cells[0].Text.ToString() + ",";
                CreateDataTable(dtRooms);
                dtRooms.Rows.Add(AddRoomEndpoint(RoomID, dtRooms, ref isError));
            }
        }

        

        selRoom += "</selected></locationList>";
        
        return selRoom;
    }
    
    protected void selectRooms(object sender, EventArgs e)
    {
        BuildRoomsXML();
        BindRooms();
    }
    
    protected void setConf(object sender, EventArgs e)
    
    {
        bool isError = false;
        ValidateEndpoints(ref isError);
        if (isError)
            return;
        setConference();
        
    }
    
    protected void BindRooms()
    {
        XDocument xd = XDocument.Parse(RoomOutXML);
        List<room> lv1s1 = new List<room>();

        if (lstConferenceType.SelectedValue == "4")
        {

            var templist = xd.Descendants("level1").Where(d => d.Element("RoomCategory").Value == "4" && d.Element("videoAvailable").Value == "0").Select(val => new room { xRoomId = val.Element("level1ID").Value }).ToList();

            lv1s1 = (from lv1 in xd.Descendants("level1").Where(d => d.Element("isVMR").Value != "1" && d.Element("ExtRoom").Value != "1" && d.Element("isTelepresence").Value != "1").OrderBy(x => x.Element("level1Name").Value)
                     select new room
                     {
                         xRoomId = Convert.ToString(lv1.Element("level1ID").Value),
                         xRoomName = Convert.ToString(lv1.Element("level1Name").Value),
                         xRoomCategory = Convert.ToString(lv1.Element("RoomCategory").Value),
                         xVideoAvailable = Convert.ToString(lv1.Element("videoAvailable").Value)
                     }).ToList();

            lv1s1 = lv1s1.Where(n => !templist.Select(n1 => n1.xRoomId).Contains(n.xRoomId)).ToList();
        }
        else if (lstConferenceType.SelectedValue == "8")
        {
            lv1s1 = (from lv1 in xd.Descendants("level1").Where(d => d.Element("RoomCategory").Value == "4").OrderBy(x => x.Element("level1Name").Value)
                     select new room
                     {
                         xRoomId = Convert.ToString(lv1.Element("level1ID").Value),
                         xRoomName = Convert.ToString(lv1.Element("level1Name").Value)
                     }).ToList();
        }
        else
        {
            var templist =xd.Descendants("level1").Where(d=>d.Element("RoomCategory").Value=="4" && d.Element("videoAvailable").Value=="0").Select(val =>new room{ xRoomId=val.Element("level1ID").Value}).ToList();

            lv1s1 = (from lv1 in xd.Descendants("level1").Where(d =>  d.Element("isVMR").Value != "1" && d.Element("ExtRoom").Value != "1").OrderBy(x => x.Element("level1Name").Value)
                     select new room
                     {
                         xRoomId = Convert.ToString(lv1.Element("level1ID").Value),
                         xRoomName = Convert.ToString(lv1.Element("level1Name").Value),
                         xRoomCategory = Convert.ToString(lv1.Element("RoomCategory").Value),
                         xVideoAvailable = Convert.ToString(lv1.Element("videoAvailable").Value)
                     }).ToList();
            
            lv1s1 = lv1s1.Where(n => !templist.Select(n1 => n1.xRoomId).Contains(n.xRoomId)).ToList();

        }


        dgRooms.DataSource = lv1s1;
        dgRooms.DataBind();
        for(int i=0; i<dgRooms.Items.Count; i++)
        {
            dgRooms.Items[i].FindControl("info").ID = dgRooms.Items[i].Cells[0].Text.ToString();
        }
                    
    }
    
    public class room
    {
        public string xRoomId { get; set; }
        public string xRoomName { get; set; }
        public string xRoomCategory { get; set; }
        public string xVideoAvailable { get; set; }

    }
        
    protected void BuildRoomsXML()
    {
        try
        {
            HttpContext.Current.Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
            StringBuilder inxml = new StringBuilder();
            inxml.Append("<conferenceTime>");
            inxml.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inxml.Append("<confID>new</confID>");
            if(chkStartNow.Checked)
                inxml.Append("<immediate>1</immediate>");
            else
                inxml.Append("<immediate>0</immediate>");

            inxml.Append("<recurring>0</recurring>");
            inxml.Append("<startDate>" + txtConfStart.Text.ToString() + "</startDate>");
            string startTime = hdnStartTime.Value.ToString();
            inxml.Append("<startHour>" + startTime.Substring(0, 2).ToString() + "</startHour>");
            inxml.Append("<startMin>" + startTime.Substring(3, 2).ToString() + "</startMin>");
            inxml.Append("<startSet>" + startTime.Substring(6, 2).ToString() + "</startSet>");
            inxml.Append("<timeZone>" + drpTimeZone.Value.ToString() + "</timeZone>");

            string setupDateTime = txtConfStart.Text.ToString() + " " + hdnStartTime.Value.ToString();
            string teardownDateTime = txtConfEnd.Text.ToString() + " " + hdnEndTime.Value.ToString();
            setupDateTime = setupDateTime.Insert(16, ":00");
            teardownDateTime = teardownDateTime.Insert(16, ":00");
            DateTime sdate = Convert.ToDateTime(setupDateTime);
            DateTime edate = Convert.ToDateTime(teardownDateTime);
            TimeSpan ts = edate - sdate;
            inxml.Append("<durationMin>" + ((ts.Hours * 60) + ts.Minutes).ToString() + "</durationMin>");
            
            //inxml.Append("<mediaType>2</mediaType>");
            inxml.Append("<confType>" + lstConferenceType.SelectedValue.ToString() + "</confType>");//FB 2334
            inxml.Append("<organizationID>11</organizationID>");
            inxml.Append("</conferenceTime>");
            RoomOutXML = obj.CallMyVRMServer("GetAvailableRoom", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

        }
        catch (Exception ex)
        {
            //log.Trace("RefreshRoom : " + ex.Message);
            ////errLabel.Text = "Error in getting Available Room(s). Please contact your VRM Administrator.";
            //errLabel.Text = obj.ShowSystemMessage();//FB 1881
            //errLabel.Visible = true;
        }
    }
    
    private void BindParticipants()
    {
        XDocument xd = XDocument.Parse(xmlstr);
        var lv1s = (from lv1 in xd.Descendants("user")
                    select new user 
                    {
                        xUserid = Convert.ToString(lv1.Element("userID").Value),
                        xFirstName = Convert.ToString(lv1.Element("firstName").Value),
                        xLastName = Convert.ToString(lv1.Element("lastName").Value),
                        xEmail = Convert.ToString(lv1.Element("email").Value)
                    });

        dgParticipants.DataSource = lv1s.ToList();
        dgParticipants.DataBind();

    }

    public class user
    {
        public string xUserid {get; set;}
        public string xFirstName { get; set; }
        public string xLastName { get; set; }
        public string xEmail { get; set; }
    }

    private void BuildParticipantsXML()
    {
        try
        {

            HttpContext.Current.Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
            String cmd = "";
            inXML.Append("<login>");
            inXML.Append("<organizationID>11</organizationID>");
            inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("<audioaddon>0</audioaddon>");
            inXML.Append("</login>");//FB 2023
            xmlstr = obj.CallMyVRMServer("GetEmailList", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027 Ends

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region ValidateEndpoints
    /// <summary>
    /// ValidateEndpoints
    /// </summary>
    /// <returns></returns>
    protected void ValidateEndpoints(ref bool isError)
    {
        try
        {             

            if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
            {
                errMsg.Text = "";
                if (hdnSelectedRoomVal.Value != "2")
                {
                    errMsg.Visible = true;
                    errMsg.Text = obj.GetTranslatedText("Only two Endpoints can be selected for Point-To-Point Conference.");
                    isError = true;                    
                }
            }                     
            
        }
        catch (Exception ex)
        {            
            errMsg.Visible = true;
            errMsg.Text = obj.GetTranslatedText(obj.ShowSystemMessage());
        }        
    }
    #endregion

    #region AddRoomEndpoint
    /// <summary>
    /// AddRoomEndpoint
    /// </summary>
    /// <returns></returns>
    protected DataRow AddRoomEndpoint(string RoomID, DataTable dt, ref bool isError)
    {
        DataRow dr = dt.NewRow();
        try
        {
            dr["ID"] = RoomID;            
            string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + RoomID + "</roomID></login>";
            string outXML = obj.CallMyVRMServer("GetOldRoom", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetOldRoom)

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);

            if (!dt.Columns.Contains("Name"))
                dt.Columns.Add("Name");
            if (!dt.Columns.Contains("EndpointName"))
                dt.Columns.Add("EndpointName");


            dr["EndpointID"] = xmldoc.SelectSingleNode("//room/endpoint").InnerText.Trim();
            dr["Name"] = xmldoc.SelectSingleNode("//room/roomName").InnerText.Trim();

            if (dr["EndpointID"].ToString() == "" || dr["EndpointID"].ToString() == "0" || dr["EndpointID"].ToString() == "-1")
            {
                if (!(lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.RoomOnly || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.HotDesking))
                {

                    dr["EndpointName"] = obj.GetTranslatedText("No Endpoint(s) associated with this room");
                    errMsg.Visible = true;
                    errMsg.Text = obj.GetTranslatedText("No Endpoint(s) associated with room: ") + dr["Name"].ToString();
                    isError = true;
                    return dr;
                }
            }
            else
            {
                inXML = "";
                inXML += "<EndpointDetails>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndpointID>" + dr["EndpointID"] + "</EndpointID>";
                inXML += "</EndpointDetails>";
                outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmlEP = new XmlDocument();
                xmlEP.LoadXml(outXML);
                if (xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile").Count > 0)
                {
                    XmlNodeList nodesEP = xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                    dr["ProfileID"] = "0";
                    dr["EndpointName"] = xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;
                    
                    foreach (XmlNode node in nodesEP)
                    {
                        if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                        {
                            if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/isTelePresence") != null && xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/isTelePresence").InnerText == "1" && lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                            {
                                errMsg.Text = obj.GetTranslatedText("Point to Point Conference should not have Telepresence endpoint.. Telepresence Room Name:")
                                            + dr["Name"].ToString();
                                errMsg.Visible = true;
                                isError = true;
                                return dr;
                            }
                        }                            
                            
                        if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                        {
                            dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                            if (dr["BridgeID"].Equals("0"))
                                dr["BridgeID"] = "-1";
                            dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;                            
                        }

                    }
                }                   
                else
                {
                    dr["EndpointID"] = "0";
                    dr["EndpointName"] = obj.GetTranslatedText("No Endpoint(s) associated with this room");
                    isError = true;
                }
            }           

        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dr;
    }
    #endregion

    #region CreateDataTable
    protected DataTable CreateDataTable(DataTable dt)
    {
        try
        {
            if (!dt.Columns.Contains("ID"))
                dt.Columns.Add("ID");
            if (!dt.Columns.Contains("Type"))
                dt.Columns.Add("Type");
            if (!dt.Columns.Contains("Name"))
                dt.Columns.Add("Name");
            if (!dt.Columns.Contains("EndpointID"))
                dt.Columns.Add("EndpointID");
            if (!dt.Columns.Contains("EndpointName"))/***/
                dt.Columns.Add("EndpointName");
            if (!dt.Columns.Contains("ProfileID"))
                dt.Columns.Add("ProfileID");
            if (!dt.Columns.Contains("Bandwidth"))
                dt.Columns.Add("Bandwidth");
            if (!dt.Columns.Contains("connectionType"))
                dt.Columns.Add("connectionType");
            if (!dt.Columns.Contains("DefaultProtocol"))
                dt.Columns.Add("DefaultProtocol");
            if (!dt.Columns.Contains("AddressType"))
                dt.Columns.Add("AddressType");
            if (!dt.Columns.Contains("Address"))
                dt.Columns.Add("Address");
            if (!dt.Columns.Contains("VideoEquipment"))
                dt.Columns.Add("VideoEquipment");
            if (!dt.Columns.Contains("BridgeID"))
                dt.Columns.Add("BridgeID");
            if (!dt.Columns.Contains("Connection"))
                dt.Columns.Add("Connection");
            if (!dt.Columns.Contains("URL"))
                dt.Columns.Add("URL");
            if (!dt.Columns.Contains("IsOutside"))
                dt.Columns.Add("IsOutside");
            if (!dt.Columns.Contains("Connect2"))
                dt.Columns.Add("Connect2");
            if (!dt.Columns.Contains("ConfCode"))
                dt.Columns.Add("ConfCode");
            if (!dt.Columns.Contains("LPin"))
                dt.Columns.Add("LPin");
            //ZD 100834 Starts
            if (!dt.Columns.Contains("APIPortNo"))
                dt.Columns.Add("APIPortNo");
            if (!dt.Columns.Contains("ExchangeID"))
                dt.Columns.Add("ExchangeID");
            if (!dt.Columns.Contains("isTelePresence"))
                dt.Columns.Add("isTelePresence");
            if (!dt.Columns.Contains("BridgeProfileID"))
                dt.Columns.Add("BridgeProfileID");
            if (!dt.Columns.Contains("AudioBridgeParty"))
                dt.Columns.Add("AudioBridgeParty");
            //ZD 100834 End
            if (!dt.Columns.Contains("IsTestEquipment")) //ZD 101815
                dt.Columns.Add("IsTestEquipment");

            return dt;
        }
        catch (Exception ex)
        {            
            errMsg.Visible = true;
            return null;
        }
    }
    #endregion
}