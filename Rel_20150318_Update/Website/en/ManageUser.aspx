<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.Users" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

 
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'Manage MCU Order', document.frmManagebridge.Bridges.value);
		
//		frmsubmit('SAVE', '');
	}
	
	function frmsubmit()
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//Added for FB 1405 -Start 
	
	function fnSearch()
	{
	    DataLoading(1);//ZD 100176
	    var obj = document.getElementById("hdnAction");
	    obj.value = 'Y';
	    return true;
	    

	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	    else
	        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
	//ZD 100176 End
	
	//Added for FB 1405 -End 
</script>
<head runat="server">
    <title>Manage Users</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
	   <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
     
    <div id="divmanageuserheight"> <%--ZD 100393--%>
      <input type="hidden" id="helpPage" value="65">
        <input type="hidden" id="hdnAction" runat="server"> <%--Added for FB 1405--%>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV"  style="display:none" align="center" >
                        <img border='0' src='image/wait1.gif'  alt='Loading..' />
                    </div> <%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext" style="margin-left:-15px"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_ExistingUsers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td width="20"></td>
                            <td align="center">
                                <table width="90%">
                                    <tr><%--ZD 100425 - Start--%>
                                        <td align="right"><span class="blackblodtext"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SortBy%>" runat="server"></asp:Literal></span>
                                            <asp:DropDownList ID="lstSortBy" CssClass="alt2SelectFormat" runat="server" AutoPostBack="true" 
											OnSelectedIndexChanged="ChangeAlphabets"  onchange="javascript:DataLoading('1');"> <%--ZD 100429--%>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ManageUser_FirstName%>"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ManageUser_LastName%>"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="<%$ Resources:WebResources, Login%>"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="<%$ Resources:WebResources, Email%>"></asp:ListItem>
                                            </asp:DropDownList> 
                                            <%--Window Dressing--%>
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_Startswith%>" runat="server"></asp:Literal></span>
                                        </td>
                                        <td align="left">
                                             <asp:Table runat="server"  ID="tblAlphabet" CellPadding="2" CellSpacing="5"></asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUsers" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false" OnItemDataBound="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUser" OnEditCommand="EditUser" OnCancelCommand="LockUser" OnUpdateCommand="RestoreUser" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="level" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="deleted" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="locked" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="firstName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ManageUser_FirstName%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="lastName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ManageUser_LastName%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Login%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="email" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Email%>"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Role%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Image ID="imgRole2" Width="25" Height="25" AlternateText="Site Administrator" ImageUrl="image/superadmin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("2"))%>' runat="server" /> <%--ZD 100419--%>
                                    <asp:Image ID="imgRole1" Width="25" Height="25" AlternateText="Administrator" ImageUrl="image/admin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("2"))%>' runat="server" /><%--ZD 100419--%>
                                    <asp:Image ID="Image2" Width="25" Height="25"  AlternateText="Administrator" ImageUrl="image/admin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("1"))%>' runat="server" /><%--ZD 100419--%>
                                    <asp:Image ID="Image1" Width="25" Height="25" AlternateText="VNOC Operator" ImageUrl="image/VNOCadmin.png" Visible='<%# (DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' runat="server" /> <%--FB 2670--%><%--FB 2816--%><%--ZD 100419--%>
                                    <asp:Image ID="imgRole0" Width="25" Height="25" AlternateText="User"  ImageUrl="image/user.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0"))%>' runat="server" /><%--ZD 100419--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemTemplate>
                                <%--ZD 100263 Start--%>
                                <% if (Session["roomCascadingControl"].ToString().Equals("1"))
                                  {%>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnEdit1%>" visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnEdit1" commandname="Edit" onclientclick="DataLoading(1)"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDelete1%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnDelete1" commandname="Delete"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnLock1%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' id="btnLock1" commandname="Cancel"></asp:LinkButton><%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDeleteInactive1%>" visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' commandname="Update" commandargument="1" id="btnDeleteInactive1"></asp:LinkButton>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnReset1%>" visible='<%# txtType.Text.Equals("3") %>' commandname="Update" commandargument="2" id="btnReset1"></asp:LinkButton>
                                 <%}
                                 else
                                 {%>
                                 <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnEdit0%>" visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") %>' enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnEdit0" commandname="Edit" onclientclick="fnCheck(this);"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%>  <%--ZD 102363--%>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDelete0%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) %>' enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' id="btnDelete0" commandname="Delete"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnLock0%>" visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") %>' enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' id="btnLock0" commandname="Cancel"></asp:LinkButton><%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnDeleteInactive0%>" visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' commandname="Update" commandargument="1" id="btnDeleteInactive0"></asp:LinkButton>
<%--                                <asp:LinkButton runat="server" Text="Delete" Visible='<%# (txtType.Text.Equals("2")) %>' CommandName="Update" CommandArgument="9" ID="btnDeleteGuest">
--%>                                <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageUser_btnReset0%>" visible='<%# txtType.Text.Equals("3") %>' commandname="Update" commandargument="2" id="btnReset0"></asp:LinkButton>
                                    <%}%>
                                 <%--ZD 100263 End--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="crossaccess" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUsers" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, NoUserFound%>" runat="server"></asp:Literal> <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                             <%--FB 2919--%>
                            <%--Windows Dressing--%>
                            <td width="40%" valign="middle" align="left" id="tdLegends" runat="server" class="blackblodtext"><%--FB 2579--%>
                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ManageUser_tdLegends%>" runat="server"></asp:Literal>
                                <font size="1" color="blue">
                                    <asp:Image ID="imgRole2" Width="25" Height="25" ImageUrl="image/superadmin.gif" runat="server" AlternateText="<%$ Resources:WebResources, ManageUser_SiteAdministra%>" /> 
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageUser_SiteAdministra%>" runat="server"></asp:Literal> <%--ZD 100419--%>
                                    <asp:Image ID="imgRole1" Width="25" Height="25" ImageUrl="image/admin.gif" runat="server" AlternateText="<%$ Resources:WebResources, Administrator%>" /> 
                                    <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, Administrator%>" runat="server"></asp:Literal> <%--ZD 100419--%>
                                    <asp:Image ID="imgRole3" Width="25" Height="25" ImageUrl="image/VNOCadmin.png" runat="server" AlternateText="<%$ Resources:WebResources, VNOCOperator%>" /> 
                                    <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, VNOCOperator%>" runat="server"></asp:Literal> <%--FB 2816--%><%--FB 2972--%> <%--ZD 100419--%>
                                    <asp:Image ID="imgRole0" Width="25" Height="25" ImageUrl="image/user.gif" runat="server" AlternateText="<%$ Resources:WebResources, User%>" /> 
                                    <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, User%>" runat="server"></asp:Literal> <%--ZD 100419--%>
                                </font>
                            </td>
                            <td align="right" width="15%">
								<%--ZD 100420--%>
                                <%--<asp:Button ID="btnDeleteAllGuest" Text="<%$ Resources:WebResources, ManageUser_btnDeleteAllGuest%>" Width="160pt" runat="server" OnClick="DeleteAllGuest"  OnClientClick="DataLoading(1)"/>--%> 
                                <button ID="btnDeleteAllGuest" style="width:160pt" runat="server" onserverclick="DeleteAllGuest"  onclick="DataLoading(1);"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ManageUser_btnDeleteAllGuest%>" runat="server"></asp:Literal></button><%--FB 2664--%><%--ZD 100176--%> <%--ZD 100420--%>
								<%--ZD 100420--%>
                        <%--Windows Dressing--%>
                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_TotalUsers%>" runat="server"></asp:Literal></b><b><asp:Label id="lblTotalUsers" runat="server" text=""></asp:Label> </b>
                            </td>
                            <td  align="right" width="15%" runat="server" id="tdLicencesRemaining">
                        <%--Windows Dressing--%>
				                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_LicensesRemain%>" runat="server"></asp:Literal></b><b><asp:Label id="lblLicencesRemaining" runat="server" text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table id="tblPage" visible="false" runat="server">
                        <asp:TableRow id="TableRow1" runat="server">
                            <asp:TableCell id="TableCell1" font-bold="True" font-names="Verdana" font-size="Small" forecolor="Blue" runat="server">
                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_Pages%>" runat="server"></asp:Literal></span> </asp:TableCell>
                            <asp:TableCell id="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr id="trSearchH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_SearchUsers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearch" runat="server">
                <td align="center">
                    <table width="80%" cellpadding="2" cellspacing="5">
                        <tr>
                             <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_FirstName%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regTemplateName" ControlToValidate="txtFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters43%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_LastName%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters43%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_EmailAddress%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtEmailAddress" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtEmailAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters44%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearchB" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td align="center"> <%--FB 2920--%>
                                <asp:Button id="btnSearchUser" onclientclick="javascript:return fnSearch();" onclick="SearchUser" runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ManageUser_btnSearchUser%>"></asp:Button> <%--Edited for FB 1405--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <%--Commented for ZD 100926--%>
            
          <%--<tr id="trNewH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUser_CreateNewUser%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr id="trNew" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td align="center"> <%--FB 2920--%>
                                <asp:Button id="btnNewMCU" onclick="CreateNewUser" runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ManageUser_CreateNewUser%>" onclientclick="DataLoading(1)"></asp:Button>  <%--ZD 100176--%> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<%--<asp:TextBox ID="txtBridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>--%> <%--ZD 100369--%>
  <input type="hidden" name="Bridges" width="200">
  <asp:TextBox ID="txtType" Visible="false" CssClass="altText" runat="server" />

<img style="display:none" src="keepalive.asp" name="myPic" width="0px" height="0px" alt="keepalive"><%--ZD 100419--%> <%--ZD 100369--%>
    </form>
    <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 Start--%>
<script language="javascript">
    if (document.getElementById('txtEmailAddress') != null)
        document.getElementById('txtEmailAddress').setAttribute("onblur", "document.getElementById('btnSearchUser').focus(); document.getElementById('btnSearchUser').setAttribute('onfocus', '');");
    if (document.getElementById('btnSearchUser') != null)
        document.getElementById('btnSearchUser').setAttribute("onblur", "document.getElementById('btnNewMCU').focus(); document.getElementById('btnNewMCU').setAttribute('onfocus', '');");

    //ZD 100393 start
    if (document.getElementById("tblNoUsers") != null)
        document.getElementById("divmanageuserheight").style.height = "400px"
    if (document.getElementById("lblTotalUsers").innerHTML == "1" || document.getElementById("lblTotalUsers").innerHTML == "2")
        document.getElementById("divmanageuserheight").style.height = "500px" // ZD 100873
    //ZD 100393 End

    //ZD 102363
    function fnCheck(Obj) {
        if (Obj.getAttribute('disabled') == "disabled")
            return false;
        else
            DataLoading(1);
    }
</script>
<%--ZD 100420 End--%>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

